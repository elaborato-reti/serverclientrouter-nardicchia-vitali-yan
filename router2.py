"""
Track1: Server-Router-Client Project

---------------ROUTER2-------------------

----------Nardicchia-Vitali-Yan----------
"""

import socket
from threading import Thread

def received_message_client(client_connection):
    """
    This function allows the router to recive message from client

    Parameters
    ----------
    client_connection : Socket
        the socket that identify the connection with the client.

    Returns
    -------
    None.

    """

    global NUM_CONNECTION


    while True:
        try:
            received_message = client_connection.recv(1024).decode("utf-8")

            source_ip = received_message[0:15]
            destination_ip = received_message[15:30]
            message = received_message[30:]

            if message == "quit":
                ARP_TABLE_SOCKET.get(source_ip).close()
                del ARP_TABLE_SOCKET[source_ip]
                NUM_CONNECTION = NUM_CONNECTION - 1
                message = source_ip+" offline"

            send_message_server(source_ip, destination_ip, message)
        except OSError:
            break

def send_message_server(source_ip, destination_ip, message):
    """
    This function allows the router to send messages to the Server

    Parameters
    ----------
    source_ip : String
        the source ip of the message.
    destination_ip : String
        the destination ip of the message.
    message : String
        the message to send.

    Returns
    -------
    None.

    """
    ethernet = destination_ip[0:8]
    if ethernet == ROUTER2_ETH0_IP[0:8]:
        source_mac = ROUTER2_ETH0_MAC
    else:
        source_mac = ROUTER2_ETH1_MAC

    destination_mac = ARP_TABLE_MAC.get(SERVER_IP)
    ethernet_header = source_mac + destination_mac
    header_ip = source_ip + destination_ip
    packet = ethernet_header + header_ip + message
    ROUTER2_SOCKET_SERVER.send(bytes(packet, "utf8"))


def received_message_server():
    """
    This function allows the toruter to recive messages from the Server

    Returns
    -------
    None.

    """
    while True:
        received_message = ROUTER2_SOCKET_SERVER.recv(1024).decode("utf-8")

        source_ip = received_message[34:49]
        destination_ip = received_message[49:64]
        message = received_message[64:]

        send_message_client(source_ip, destination_ip, message)


def send_message_client(source_ip, destination_ip, message):
    """
    This function allows the router to send messages to clients

    Parameters
    ----------
    source_ip : String
        the source ip of the message.
    destination_ip : String
        the destination ip of the message.
    message : String
        the message to send.

    Returns
    -------
    None.
    """

    if ARP_TABLE_SOCKET.get(destination_ip) is None:
        message = destination_ip+" is offline"
        destination_ip = source_ip
        ethernet = destination_ip[0:8]
        if ethernet == ROUTER2_ETH0_IP[0:8]:
            send_message_server(ROUTER2_ETH0_IP, destination_ip, message)
        else:
            send_message_server(ROUTER2_ETH1_IP, destination_ip, message)
    else:
        source_mac = ROUTER2_ETH1_MAC
        destination_mac = ARP_TABLE_MAC.get(destination_ip)
        ethernet_header = source_mac + destination_mac
        header_ip = source_ip + destination_ip
        packet = ethernet_header + header_ip + message
        ARP_TABLE_SOCKET.get(destination_ip).send(bytes(packet, "utf8"))


def accept_connection():
    """
    This function accepts connections from clients

    Returns
    -------
    None.

    """
    global NUM_CONNECTION

    while True:
        client_connection, adress = ROUTER2_SOCKET_CLIENT.accept()
        client_ip = client_connection.recv(1024).decode("utf-8")
        ARP_TABLE_SOCKET[client_ip] = client_connection
        NUM_CONNECTION = NUM_CONNECTION + 1
        send_message_server(client_ip, SERVER_IP, client_ip+" online")

        Thread(target=received_message_client, args=(client_connection, )).start()

ROUTER2_SOCKET_CLIENT = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
ROUTER2_SOCKET_CLIENT.bind(("localhost", 8500))

ROUTER2_SOCKET_SERVER = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
ROUTER2_SOCKET_SERVER.bind(("localhost", 8400))

#eth0
ROUTER2_ETH0_IP = "195.001.010.002"
ROUTER2_ETH0_MAC = "32:03:0A:CF:10:DB"

#eth1
ROUTER2_ETH1_IP = "001.005.010.001"
ROUTER2_ETH1_MAC = "32:03:0A:DA:11:DC"

CLIENT4_IP = "001.005.010.015"
CLIENT4_MAC = "42:A3:1B:DA:12:AC"

CLIENT5_IP = "001.005.010.020"
CLIENT5_MAC = "32:03:0A:DA:11:DC"

CLIENT6_IP = "001.005.010.030"
CLIENT6_MAC = "44:BF:5B:DA:11:AC"


SERVER_IP = "195.001.010.010"
SERVER_MAC = "52:AB:0A:DF:10:DC"

SERVER = ("localhost", 8000)
ROUTER2_SOCKET_SERVER.connect(SERVER)

ROUTER2_SOCKET_CLIENT.listen(4)

ARP_TABLE_SOCKET = {}
ARP_TABLE_MAC = {SERVER_IP : SERVER_MAC, CLIENT4_IP : CLIENT4_MAC,
                 CLIENT5_IP : CLIENT5_MAC, CLIENT6_IP : CLIENT6_MAC}

NUM_CONNECTION = 1

CLIENT_THREAD = Thread(target=accept_connection)
CLIENT_THREAD.start()
SERVER_THREAD = Thread(target=received_message_server)
SERVER_THREAD.start()
CLIENT_THREAD.join()
SERVER_THREAD.join()
