"""
Track1: Server-Router-Client Project

---------------SERVER-------------------

----------Nardicchia-Vitali-Yan---------
"""

import socket
from threading import Thread

def receive_message(router_connection):
    """
    The function allows you to receive messages from your router.

    Parameters
    -------
    router_connection: Socket
        the socket of the router

    Returns
    -------
    None.

    """

    while True:
        #riceve il messaggio dal socket connesso
        received_message = router_connection.recv(1024).decode("utf-8")
        #il pacchetto viene spacchettato e si prendono i parametri
        source_ip = received_message[34:49]
        destination_ip = received_message[49:64]
        message = received_message[64:]
        #se la destinazione del messaggio non è il server
        if not destination_ip == SERVER_IP:
            #chiama la funzione send_message per mandare il messaggio
            send_message(source_ip, destination_ip, message)
        else:
            #altrimenti il messaggio è stato inviato direttamente al server per notificare qualcosa
            print(message)

def identify_router_connection(router, source_ip, destination_ip, message):
    """
    The function allows you to identify the router connection.

    Parameters:
    -------
    router: String
            the router to identify
    source_ip: String
            the source ip address of the packet
    destination_ip: String
            the destination ip address of the packet
    message: String
            the message received in the packet

    Returns
    -------
    destination_mac, destination_socket, message: String, Socket, String
            parameters to set the packet

    """

     #se il router non è ancora connesso
    if router not in CONNECTION.keys():
        #imposta il messaggio in un messaggio di offline
        message = destination_ip + " is offline"
        #cambia l'indirizzo destinatario all'indirizzo mittente
        destination_ip = source_ip
        if router == "router1":
            destination_socket = CONNECTION["router2"]
            destination_mac = ROUTER_MAC.get("router2")
        else:
            destination_socket = CONNECTION["router1"]
            destination_mac = ROUTER_MAC.get("router1")
    else: #altrimenti se il router è connesso
        destination_socket = CONNECTION[router]
        destination_mac = ROUTER_MAC.get(router)
    return destination_mac, destination_socket, message

def send_message(source_ip, destination_ip, message):
    """
    The function allows you to send messages.

    Parameters:
    -------
    source_ip: String
            the source ip address
    destination_ip: String
            the destination ip address to send the message
    message: String
            the message to send

    Returns
    -------
    None.

    """

    source_mac = SERVER_MAC
    header_ip = source_ip + destination_ip
    #abbiamo bisogno di sapere a quale router appartiene il destinatario
    ethernet = destination_ip[0:8]
    #si identifica il router a cui mandare il pacchetto
    #se l'ethernet del destinatario è uguale all'ethernet del router1
    if ethernet == ROUTER1_ETH0_IP[0:8]:
        destination_mac, destination_socket, message = \
        identify_router_connection("router1", source_ip, destination_ip, message)
    #altrimenti se l'ethernet del destinatario è uguale all'ethernet del router2
    else:
        destination_mac, destination_socket, message = \
        identify_router_connection("router2", source_ip, destination_ip, message)

    ethernet_header = source_mac + destination_mac
    #crea un nuovo pacchetto
    packet = ethernet_header + header_ip + message
    #invia il pacchetto al router
    destination_socket.send(bytes(packet, "utf-8"))

def accept_connection():
    """
    The function allows to accept connections from routers.

    Returns
    -------
    None.

    """

    while True:
        #accetta la connessione
        router_connection, address = SERVER_SOCKET.accept()
        if address[1] == 8100:
            #memorizza la connessione del router1 nel dizionario
            CONNECTION["router1"] = router_connection
        else:
            #memorizza la connessione del router2 nel dizionario
            CONNECTION["router2"] = router_connection
        print("%s : %s connect" %address)
        #si mette in attesa di ricevere i pacchetti dal router
        Thread(target=receive_message, args=(router_connection, )).start()

#indirizzo ip e mac del server
SERVER_IP = "195.001.010.010"
SERVER_MAC = "52:AB:0A:DF:10:DC"

#indirizzi ip e mac degli ethernet 0 e 1 dei router
ROUTER1_ETH0_IP = "092.010.010.001"
ROUTER1_ETH0_MAC = "55:04:0A:EF:11:CF"
ROUTER1_ETH1_IP = "195.001.010.001"
ROUTER1_ETH1_MAC = "55:04:0A:EF:10:AB"

ROUTER2_ETH0_IP = "195.001.010.002"
ROUTER2_ETH0_MAC = "32:03:0A:CF:10:DB"
ROUTER2_ETH1_IP = "001.005.010.001"
ROUTER2_ETH1_MAC = "32:03:0A:DA:11:DC"

SERVER_SOCKET = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
#associamo un nuovo socket server da localhost sulla porta 8000
SERVER_SOCKET.bind(("localhost", 8000))
#abilita il socket a ricevere connessioni
SERVER_SOCKET.listen(4)

#dizionario per memorizzare i router.
CONNECTION = {}
#dizionario per memorizzare i mac dei router.
ROUTER_MAC = {"router1" :  ROUTER1_ETH0_MAC, "router2" : ROUTER2_ETH1_MAC}

ACCEPT_THREAD = Thread(target=accept_connection)
ACCEPT_THREAD.start()
ACCEPT_THREAD.join()
