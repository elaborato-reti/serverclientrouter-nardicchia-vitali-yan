# README #

## TRACCIA 1 ##
#### Componenti del gruppo: ####
Nome   | Cognome    | Matricola  |
-------|------------|------------|
Giulia | Nardicchia | 0000882043 |
Anna   | Vitali     | 0000873655 |
Elena  | Yan        | 0000880506 |

## Architettura della rete ##
![rete](images/rete.png)

## ISTRUZIONI ##
Eseguire per primo il file `server.py` dopodichè è possibile far partire i due router attraverso i file `router1.py` e `router2.py`, non è necessario che siano entrambi collegati perchè è possibile scambiare messaggi anche fra client della stessa rete senza coinvolgere l'altro router ma solo quello a cui i client si interfacciano. Infine per inziare lo scambio dei messaggi occorre eseguire i file dei client che fanno riferimento al router o ai router già connessi. Quando viene fatto partire un client si attiva un' interfaccia grafica in cui è possibile scrivere un messaggio nella casella di testo e inviarlo tramite il tasto **invio** o il pulsante **send**, presente sull'interfaccia; i messaggi ricevuti saranno visibili nella casella di scorrimento dell'interfaccia.
L’utente ha due modi diversi per uscire, tramite la chiusura dell’interfaccia grafica o inserendo la stringa **quit** nella casella di testo.
Nei due router dopo che sono stati fatti partire non si visualizzerà nessun messaggio nella console, nel server vengono mostrati i router connessi, i client online e quelli offline dopo che sono usciti, invece nei client è possibile visualizzare i pacchetti ricevuti.

![interfaccia](images/client1.PNG)

## CLIENT ##
I client sono suddivisi in due reti : i client 1,2,3 appartengono a eth0 e sono interfacciati con il router1 mentre i client 4,5,6 appartengono a eth1 e sono interfacciati con il router2.
I compiti principali dei client sono quelli di spedire e ricevere messaggi.
Il client viene creato tramite socket e si connette al router, inizialmente invia il suo indirizzo IP al router in modo tale da notificare la sua presenza nella rete (è online).
A questo punto siccome è attivo in rete potrà mandare un messaggio ad un altro client.
Se quest’ultimo è anch’esso online riceverà il messaggio, altrimenti verrà inviata al client mittente la notifica che il destinatario è offline.
Quando l'utente esce, il client invia un messaggio al server per notificare la sua uscita dalla rete (è offline), si chiude il socket e la finestra.

## ROUTER ##
La rete è costituita da due router uno che si interfaccia con i client 1, 2 e 3 l'altro invece che si interfaccia con i client 4, 5 e 6.
Il primo compito che svolge un router quando un client  si connette è notificarlo al Server, infatti, non appena viene stabilita una connessione con un client il router invia un messaggio al server indicandogli che lo specifico client è online.  Una volta fatto questo il ruouter aspetta di ricevere messaggi dai client attualmente connessi. Il compito principale dei router è proprio quello di consentire lo scambio di messaggi fra i diversi client della rete; quando un client connesso chiede di inviare un messaggio, a un certo destinatario, il router determina la rete che deve essere utilizzata per inviare il messaggio se eth0 o eth1, dopodichè invia il messaggio al Server ,in quanto seguendo la traccia, quando il client vuole inviare un messaggio questo viene inviato al server che tiene traccia di tutti i messaggi che sono stati scambiati, il quale poi invierà il messaggio al router di competenza, quando invece il router riceve un messaggio dal server affinchè esso lo recapiti al destinatario possono esserci due scenari possibili: o il client è connesso, e il messaggio li viene recapitato o il client non è connesso  e in questo caso il router scrive un messaggio indicando che il client cercato non è online e lo spedisce al mittente del precedente messaggio tramite il server. Infine, quando invece, un client richiede di uscire dalla rete, tramite il messaggio "quit" o tramite la chiusura dell'interfaccia, il router, si occupa di notificare al server, tramite l'invio di un messaggio, che lo specifico client è offline

## SERVER ##
Il server per prima cosa associa un nuovo socket server da localhost sulla porta 8000.
Al server viene associato l’indirizzo ip “95.1.10.10” e l’indirizzo mac "52:AB:0A:DF:10:DC", inoltre il server conosce anche gli indirizzi ip e mac dei router perché è direttamente collegato ad essi.
Prima di tutto il server si mette in attesa di ricevere richieste di connessione da parte dei router e poi di accettarle. Viene fatto partire un thread per ricevere pacchetti da ogni router connesso.
Quando un client invia un messaggio a un altro client, questo messaggio prima viene passato al router che poi viene inviato al server.
Il server, una volta ricevuto un pacchetto da un router, scompatta il pacchetto per sapere chi è il client destinatario e di conseguenza ricavare l’ethernet per inviare il pacchetto al router interessato.
Se il router dove è presente il destinatario non è connesso e quindi anche il client destinatario non è connesso, il server restituisce un messaggio al client mittente per informare che il client destinatario non è online.
