"""
Track1: Server-ROUTER-Client Project

---------------CLIENT3-------------------

----------Nardicchia-Vitali-Yan----------
"""

import socket
import sys
from threading import Thread
import tkinter as tkt
from tkinter import ttk
from tkinter import scrolledtext

def send_message(event=None):
    """
    Manage the sending of the message.

    Parameters
    ----------
    event : TYPE, optional
        DESCRIPTION. The default is None.

    Returns
    -------
    None.

    """

    message = MESSAGES.get()
    MESSAGES.set("")

    if message == "quit":
        close_event()

    destination_ip = TABLE.get(COMBO.get())

    header_ip = CLIENT3_IP + destination_ip
    packet = header_ip + message

    CLIENT3_SOCKET.send(bytes(packet, "utf8"))


def received_message():
    """
    Manage the reception of the message.

    Returns
    -------
    None.

    """
    while True:
        try:
            message_received = CLIENT3_SOCKET.recv(1024).decode("utf-8")

            source_mac = message_received[0:17]
            destination_mac = message_received[17:34]
            source_ip = message_received[34:49]
            destination_ip = message_received[49:64]
            message = message_received[64:]

            message_insert = "Source MAC address: {source_mac}"\
            .format(source_mac=source_mac) + \
            "\nDestination MAC address: {destination_mac}"\
            .format(destination_mac=destination_mac) + \
            "\nSource IP address: {source_ip} \nDestination IP address: {destination_ip}"\
            .format(source_ip=source_ip, destination_ip=destination_ip) + \
            "\nMessage: " + message + \
            "\n--------------------------------------------------\n"

            MESSAGES_RECEIVED.insert(tkt.INSERT, message_insert)

            print("\nThe packed received: " + message_insert)

        except:
            break


def close_event():
    """
    Event resulting from the closure of the application.

    Returns
    -------
    None.

    """
    message = "quit"
    destination_ip = SERVER_IP
    header_ip = CLIENT3_IP + destination_ip
    packet = header_ip + message

    CLIENT3_SOCKET.send(bytes(packet, "utf8"))

    CLIENT3_SOCKET.close()
    WINDOW.quit()
    WINDOW.destroy()
    sys.exit(0)

WINDOW = tkt.Tk()
WINDOW.title("client 3")
WINDOW.geometry('450x350')

MESSAGES_FRAME = tkt.Frame(WINDOW)
MESSAGES = tkt.StringVar()
MESSAGES_FRAME.pack()

tkt.Label(WINDOW, text="Write your message or \"quit\" to exit: ").pack()

MESSAGES_FIELD = tkt.Entry(WINDOW, textvariable=MESSAGES)
MESSAGES_FIELD.bind("<Return>", send_message)
MESSAGES_FIELD.focus()
MESSAGES.set("")
MESSAGES_FIELD.pack()

tkt.Label(WINDOW, text="Choose the receiver: ").pack()

COMBO = ttk.Combobox(WINDOW, state='readonly')
COMBO['values'] = ("client 1", "client 2", "client 3", "client 4", "client 5", "client 6")
COMBO.current(2)
COMBO.pack()

tkt.Button(WINDOW, text="Send", command=send_message).pack()

tkt.Label(WINDOW, text="Received MESSAGES: ").pack()

MESSAGES_RECEIVED = scrolledtext.ScrolledText(WINDOW, width=50, height=12)
MESSAGES_RECEIVED.pack()

WINDOW.protocol("WM_DELETE_WINDOW", close_event)

CLIENT3_IP = "092.010.010.025"
CLIENT3_MAC = "AF:04:67:EF:19:DA"

SERVER_IP = "195.001.010.010"
ROUTER = ("localhost", 8200)

TABLE = {"client 1" : "092.010.010.015",\
         "client 2" : "092.010.010.020",\
         "client 3" : CLIENT3_IP,\
         "client 4" : "001.005.010.015",\
         "client 5" : "001.005.010.020",\
         "client 6" : "001.005.010.030"}

CLIENT3_SOCKET = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
CLIENT3_SOCKET.connect(ROUTER)
CLIENT3_SOCKET.send(bytes(CLIENT3_IP, "utf8"))

RECEIVE_THREAD = Thread(target=received_message)
RECEIVE_THREAD.start()
WINDOW.mainloop()
RECEIVE_THREAD.join()
