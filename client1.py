"""
Track1: Server-ROUTER-Client Project

---------------CLIENT1-------------------

----------Nardicchia-Vitali-Yan----------
"""

import socket
import sys
from threading import Thread
import tkinter as tkt
from tkinter import ttk
from tkinter import scrolledtext

def send_message(event=None):
    """
    Manage the sending of the message.

    Parameters
    ----------
    event : TYPE, optional
        DESCRIPTION. The default is None.

    Returns
    -------
    None.

    """

    #prende il messaggio dal campo di testo
    message = MESSAGES.get()
    #svuota il campo di testo per permettere la scrittura del messaggio successivo
    MESSAGES.set("")

    #controllo se l'utente ha digitato la stringa "quit"
    if message == "quit":
        close_event()

    #prende, dalla casella combinata, l'ip di destinazione associato al client selezionato
    destination_ip = TABLE.get(COMBO.get())

    #impacchetta il messaggio
    header_ip = CLIENT1_IP + destination_ip
    packet = header_ip + message

    #invio del pacchetto
    CLIENT1_SOCKET.send(bytes(packet, "utf8"))


def received_message():
    """
    Manage the reception of the message.

    Returns
    -------
    None.

    """
    while True:
        try:
            #ricezione del messaggio
            message_received = CLIENT1_SOCKET.recv(1024).decode("utf-8")

            #il pacchetto viene spacchettato
            source_mac = message_received[0:17]
            destination_mac = message_received[17:34]
            source_ip = message_received[34:49]
            destination_ip = message_received[49:64]
            message = message_received[64:]

            message_insert = "Source MAC address: {source_mac}"\
            .format(source_mac=source_mac) + \
            "\nDestination MAC address: {destination_mac}"\
            .format(destination_mac=destination_mac) + \
            "\nSource IP address: {source_ip} \nDestination IP address: {destination_ip}"\
            .format(source_ip=source_ip, destination_ip=destination_ip) + \
            "\nMessage: " + message + \
            "\n--------------------------------------------------\n"

            #il messaggio viene visualizzato nella casella di testo a scorrimento
            MESSAGES_RECEIVED.insert(tkt.INSERT, message_insert)

            #il messaggio viene visualizzato sulla console
            print("\nThe packed received: " + message_insert)

        except:
            break


def close_event():
    """
    Event resulting from the closure of the application.

    Returns
    -------
    None.

    """
    #viene creato un pacchetto da mandare al server per notificare l'uscita
    message = "quit"
    destination_ip = SERVER_IP
    header_ip = CLIENT1_IP + destination_ip
    packet = header_ip + message

    #invio del pacchetto
    CLIENT1_SOCKET.send(bytes(packet, "utf8"))

    #chiusura socket e finestra
    CLIENT1_SOCKET.close()
    WINDOW.quit()
    WINDOW.destroy()
    sys.exit(0)

#creazione della finestra
WINDOW = tkt.Tk()
#titolo della finestra
WINDOW.title("client 1")
#settaggio delle dimensioni della finestra
WINDOW.geometry('450x350')

#creazione del frame per contenere i messaggi
MESSAGES_FRAME = tkt.Frame(WINDOW)
#creazione di una variabile di tipo stringa per i messaggi da inviare
MESSAGES = tkt.StringVar()
MESSAGES_FRAME.pack()

tkt.Label(WINDOW, text="Write your message or \"quit\" to exit: ").pack()

#creazione del campo in cui inserire il messaggio che si vuole inviare
MESSAGES_FIELD = tkt.Entry(WINDOW, textvariable=MESSAGES)
#associa il tasto invio alla funzione send_message
MESSAGES_FIELD.bind("<Return>", send_message)
#focalizzare l'attenzione dell'utente, tramite cursore, nel campo in cui si inserisce il messaggio
MESSAGES_FIELD.focus()
#settaggio iniziale della stringa
MESSAGES.set("")
MESSAGES_FIELD.pack()

tkt.Label(WINDOW, text="Choose the receiver: ").pack()

#creazione della casella combinata contenente tutti i possibili destinatari
COMBO = ttk.Combobox(WINDOW, state='readonly')
COMBO['values'] = ("client 1", "client 2", "client 3", "client 4", "client 5", "client 6")
#seleziona il primo elemento di default
COMBO.current(0)
COMBO.pack()

#creazione del bottone per l'invio del messaggio
tkt.Button(WINDOW, text="Send", command=send_message).pack()

tkt.Label(WINDOW, text="Received MESSAGES: ").pack()

#creazione della casella di testo a scorrimento per la ricezione dei messaggi
MESSAGES_RECEIVED = scrolledtext.ScrolledText(WINDOW, width=50, height=12)
MESSAGES_RECEIVED.pack()

WINDOW.protocol("WM_DELETE_WINDOW", close_event)

#indirizzi ip e mac del client1
CLIENT1_IP = "092.010.010.015"
CLIENT1_MAC = "32:04:0A:EF:19:CF"

#indirizzo ip del server
SERVER_IP = "195.001.010.010"
#porta del ROUTER
ROUTER = ("localhost", 8200)

TABLE = {"client 1" : CLIENT1_IP,\
         "client 2" : "092.010.010.020",\
         "client 3" : "092.010.010.025",\
         "client 4" : "001.005.010.015",\
         "client 5" : "001.005.010.020",\
         "client 6" : "001.005.010.030"}

#creazione del socket del client1
CLIENT1_SOCKET = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
#connessione al ROUTER tramite porta
CLIENT1_SOCKET.connect(ROUTER)
CLIENT1_SOCKET.send(bytes(CLIENT1_IP, "utf8"))

RECEIVE_THREAD = Thread(target=received_message)
RECEIVE_THREAD.start()
WINDOW.mainloop()
RECEIVE_THREAD.join()
