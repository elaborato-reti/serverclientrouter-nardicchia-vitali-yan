"""
Track1: Server-Router-Client Project

---------------ROUTER1-------------------

----------Nardicchia-Vitali-Yan----------
"""

import socket
from threading import Thread

def received_message_client(client_connection):
    """
    This function allows the router to recive message from client

    Parameters
    ----------
    client_connection : Socket
        the socket that identify the connection with the client.

    Returns
    -------
    None.

    """

    global NUM_CONNECTION

    while True:
        try:
            received_message = client_connection.recv(1024).decode("utf-8")
            #si elabora il pacchetto
            source_ip = received_message[0:15]
            destination_ip = received_message[15:30]
            message = received_message[30:]

            if message == "quit":
                #si chiude la connessione
                client_connection.close()
                del ARP_TABLE_SOCKET[source_ip]
                #si decrementa il numero di connessioni
                NUM_CONNECTION = NUM_CONNECTION - 1
                message = source_ip+" offline"
            #si invia il messaggio al server
            send_message_server(source_ip, destination_ip, message)
        except OSError:
            break

def send_message_server(source_ip, destination_ip, message):
    """
    This function allows the router to send messages to the Server

    Parameters
    ----------
    source_ip : String
        the source ip of the message.
    destination_ip : String
        the destination ip of the message.
    message : String
        the message to send.

    Returns
    -------
    None.

    """
    #viene impostato l'indirizzo mac sorgente del pacchetto
    ethernet = destination_ip[0:8]
    if ethernet == ROUTER1_ETH0_IP[0:8]:
        source_mac = ROUTER1_ETH0_MAC
    else:
        source_mac = ROUTER1_ETH1_MAC
    #viene assmeblato il pacchetto
    destination_mac = ARP_TABLE_MAC.get(SERVER_IP)
    ethernet_header = source_mac + destination_mac
    header_ip = source_ip + destination_ip
    packet = ethernet_header + header_ip + message
    #viene inviato il pacchetto al server
    ROUTER1_SOCKET_SERVER.send(bytes(packet, "utf8"))

def received_message_server():
    """
    This function allows the toruter to recive messages from the Server

    Returns
    -------
    None.

    """
    while True:
        received_message = ROUTER1_SOCKET_SERVER.recv(1024).decode("utf-8")
        #si elabora il pacchetto ricevuto
        source_ip = received_message[34:49]
        destination_ip = received_message[49:64]
        message = received_message[64:]
        #invia il messaggio al client destinatario
        send_message_client(source_ip, destination_ip, message)

def send_message_client(source_ip, destination_ip, message):
    """
    This function allows the router to send messages to clients

    Parameters
    ----------
    source_ip : String
        the source ip of the message.
    destination_ip : String
        the destination ip of the message.
    message : String
        the message to send.

    Returns
    -------
    None.

    """
    if ARP_TABLE_SOCKET.get(destination_ip) is None:
        #se il destinatario non è connesso si manda un messagio al mittente tramite il server
        message = destination_ip+" is offline"
        destination_ip = source_ip
        ethernet = destination_ip[0:8]
        #controllo della rete a cui inviare il messaggio
        if ethernet == ROUTER1_ETH0_IP[0:8]:
            #invia il messaggio al server sulla eth0
            send_message_server(ROUTER1_ETH0_IP, destination_ip, message)
        else:
            #invia il messaggio al server sulla eth1
            send_message_server(ROUTER1_ETH1_IP, destination_ip, message)
    else:
        #si invia il messaggio al destinatario, creando un nuovo pacchetto
        source_mac = ROUTER1_ETH0_MAC
        destination_mac = ARP_TABLE_MAC.get(destination_ip)
        ethernet_header = source_mac + destination_mac
        header_ip = source_ip + destination_ip
        packet = ethernet_header + header_ip + message
        ARP_TABLE_SOCKET.get(destination_ip).send(bytes(packet, "utf8"))

def accept_connection():
    """
    This function accepts connections from clients

    Returns
    -------
    None.

    """
    global NUM_CONNECTION

    while True:
        client_connection, address = ROUTER1_SOCKET_CLIENT.accept()
        client_ip = client_connection.recv(1024).decode("utf-8")
        #si aggiorna la tabella con la nuova connesisone
        ARP_TABLE_SOCKET[client_ip] = client_connection
        #si incrementa il numero di connessioni
        NUM_CONNECTION = NUM_CONNECTION + 1
        #si notifica al server che lo specifico client è online
        send_message_server(client_ip, SERVER_IP, client_ip+" online")
        #si mette in attesa di ricevere messaggi dal client
        Thread(target=received_message_client, args=(client_connection, )).start()

#socket per ricevere e inviare messaggi al client
ROUTER1_SOCKET_CLIENT = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
ROUTER1_SOCKET_CLIENT.bind(("localhost", 8200))

#socket per ricevere e inviare messaggi al server
ROUTER1_SOCKET_SERVER = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
ROUTER1_SOCKET_SERVER.bind(("localhost", 8100))

#eth0
ROUTER1_ETH0_IP = "092.010.010.001"
ROUTER1_ETH0_MAC = "55:04:0A:EF:11:CF"
#eth1
ROUTER1_ETH1_IP = "195.001.010.001"
ROUTER1_ETH1_MAC = "55:04:0A:EF:10:AB"

CLIENT1_IP = "092.010.010.015"
CLIENT1_MAC = "32:04:0A:EF:19:CF"

CLIENT2_IP = "092.010.010.020"
CLIENT2_MAC = "10:AF:CB:EF:19:CF"

CLIENT3_IP = "092.010.010.025"
CLIENT3_MAC = "AF:04:67:EF:19:DA"

SERVER_IP = "195.001.010.010"
SERVER_MAC = "52:AB:0A:DF:10:DC"

SERVER = ("localhost", 8000)
ROUTER1_SOCKET_SERVER.connect(SERVER)

ROUTER1_SOCKET_CLIENT.listen(5)

#tabella che conterrà gli indirizzi ip e le connessioni
ARP_TABLE_SOCKET = {}
#tabella che conterrà gli indirizzi ip e gli indirizzi mac
ARP_TABLE_MAC = {SERVER_IP : SERVER_MAC, CLIENT1_IP : CLIENT1_MAC,
                 CLIENT2_IP : CLIENT2_MAC, CLIENT3_IP:CLIENT3_MAC}

#indica il numero di host connessi attualmente, per ora solo il server
NUM_CONNECTION = 1

CLIENT_THREAD = Thread(target=accept_connection)
CLIENT_THREAD.start()
SERVER_THREAD = Thread(target=received_message_server)
SERVER_THREAD.start()
CLIENT_THREAD.join()
SERVER_THREAD.join()
